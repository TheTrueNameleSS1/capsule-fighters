// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "CapsuleFightersGameModeBase.generated.h"

/**
 * 
 */
UCLASS()
class CAPSULEFIGHTERS_API ACapsuleFightersGameModeBase : public AGameModeBase
{
	GENERATED_BODY()
	
};
